#/usr/bin/perl

use Dancer ':script';
use Dancer::Plugin::Database;
use Data::Dumper;
use MediaWiki::API;
use DateTime;
use DateTime::Format::Strptime;
use DateTime::Event::Recurrence;
use utf8;

# use Dancer::Plugin::NYTProf; # do not run profiler unless we need it

$|++;    # forces a flush after every write or print

my %known_wikis = (
    'ru.wikinews.org' => 'Опубликовано',
    'en.wikinews.org' => 'Published',
);

for my $wiki ( keys %known_wikis ) {
    print "Working on $wiki...\n";
    my $cat_name = $known_wikis{$wiki};
    print "* Category name $cat_name...\n";
    my $wiki_url = $wiki;
    $wiki =~ s/\.//g;
    print "* Db name $wiki...\n";

    my $mw = MediaWiki::API->new();
    $mw->{config}->{api_url} = "https://$wiki_url/w/api.php";

    my $strp = DateTime::Format::Strptime->new(
        pattern   => '%Y-%m-%dT%H:%M:%SZ',
        locale    => 'en_AU',
        time_zone => 'UTC',
    );

    my $strp_db_days = DateTime::Format::Strptime->new(
        pattern => '%F',  # Equivalent to %Y-%m-%d. (This is the ISO style date)
        locale => 'en_AU',
    );
    print "* Created the needed variables...\n";
    print "* Creating database $wiki...\n";

    my $createdb = database->do(
"CREATE TABLE IF NOT EXISTS $wiki (date VARCHAR(12) PRIMARY KEY, count INTEGER)"
    );
    print "* Created the database if it didn't exist...\n";

    #
    # populate or update the database
    #

    my %data;
    my @years = ( 2005 .. 2013 );
    my @months = ( 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12 );

    # retrieve last saved date
    my $get_latest_date =
      database->prepare("select date from $wiki order by date desc limit 1");
    $get_latest_date->execute();
    my $row         = $get_latest_date->fetchrow_hashref;
    my $latest_date = $row
      ? $strp_db_days->parse_datetime( $row->{date} )
      ->add( days => 1 )    # don't get stats if we already have them
      : DateTime->new( year => 2005, month => 01, day => 01 );

    # process yearly lists of published articles
    for my $y (@years) {
        next if $y < $latest_date->year;
        print "* getting ts... ";

        # earliest possible: last saved date
        my $startTime =
          ( $latest_date->year eq $y )
          ? $strp->format_datetime($latest_date)
          : "$y:01:01 00:00:00";

        # latest possible: today
        my ( $h, $m, $s ) =
          ( DateTime->now->hour, DateTime->now->minute, DateTime->now->second );
        my $endTime = ( $y eq DateTime->now->year )
          ? $strp->format_datetime( ( DateTime->now )
            ->add( hours => -$h, minutes => -$m, seconds => -$s - 1 )
          )    # don't get stats for today
          : "$y:12:31 23:59:59";

        print "done.\n";

        # retrieve articles list for a year
        print "* from $startTime to $endTime. getting data... ";
        my $articles = $mw->list(
            {
                action      => 'query',
                list        => 'categorymembers',
                cmtitle     => "Category:$cat_name",
                cmnamespace => 0,
                cmlimit     => 5000,
                cmsort      => 'timestamp',
                cmdir       => 'desc',
                cmstart     => $endTime,
                cmend       => $startTime,
                cmprop      => "timestamp",
            }
        ) or die $mw->{error}->{code} . ': ' . $mw->{error}->{details};

        # put the daily articles count into %data
        print "done.\n* counting ... ";
        for my $article ( @{$articles} ) {
            my $ts   = $article->{timestamp};
            my $time = $strp->parse_datetime($ts);   # This is a DateTime object
            $data{
                sprintf( "%d-%02d-%02d",
                    ( $time->year, $time->month, $time->day ) )
            }++;
        }
        print "done.\n";
    }

    database->begin_work;                            # works with AutoCommit 1
    eval {
        for my $key ( keys %data ) {

            # insert count into the database
            my $add_statement =
              database->prepare("INSERT INTO $wiki VALUES (?, ?)");
            $add_statement->execute( $key, $data{$key} );
        }
        database->commit;    # commit the changes if we get this far
    };
    if ($@) {
        warn "Transaction aborted because $@";

        # now rollback to undo the incomplete changes
        # but do it in an eval{} as it may also fail
        eval { database->rollback };

        # add other application on-error-clean-up code here
    }

}
