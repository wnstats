package Wnstats;
use Dancer ':syntax';
use Dancer::Plugin::Ajax;
use DBIx::Class::Schema::Loader;
use DBIx::Class;
use Dancer::Plugin::Database;
use DateTime;
use DateTime::Event::Recurrence;
use Data::Dumper;

ajax '/info/sites' => sub {
    my $tables_array = database->table_info('main','main', '%','TABLE')->fetchall_arrayref;
    my @tables = ();
    for my $table_info (@{$tables_array}){
        push @tables, $table_info->[2];
    }
    return { sites => \@tables };
};

sub num_published_month_before {
    my ( $y0, $m0, $d0, $home_wiki ) = @_;
    $home_wiki =~ s/\.//g;
    my $date = sprintf( "%d-%02d-%02d", ( $y0, $m0, $d0 ) );
    my $driver_name = database->{Driver}->{Name};
    my $str_query;
    if ( $driver_name eq 'mysql' ) {
        $str_query =
            'select SUM(count) from '
          . $home_wiki
          . ' where date < ? and date > ADDDATE(?,-31)';
    }
    elsif ( $driver_name eq 'SQLite' ) {
        $str_query =
            'select SUM(count) from '
          . $home_wiki
          . ' where date < ? and date > DATE(?,"-1 month")';
    }
    my ($n) = database->selectrow_array( $str_query, {}, $date, $date );
    my $count = $n ? $n : 0;
    return $count;
}

sub get_chart_points {
    my ( $years, $interval, $home_wiki ) = @_;    # interval in days, <=30
    my @points;

    # get the abscissae using start date, end date, interval
    # using DateTime::Event::Recurrence
    my $dt1 = DateTime->new( year => $years->[0],  month => 1,  day => 1 );
    my $dt2 = DateTime->new( year => $years->[-1], month => 12, day => 31 );

    if ( $dt2 > DateTime->now ) {
        $dt2 = DateTime->now;
    }

    my $daily_set = DateTime::Event::Recurrence->daily( interval => $interval );
    my @days = $daily_set->as_list( start => $dt1, end => $dt2 );

    # for each abscissae, retrieve the ordinate ...
    for my $day (@days) {
        last if $day->truncate( to => "day" ) == DateTime->today;

        # ... by checking the db for total last 30 days articles count
        my ( $y, $m, $d ) = ( $day->year, $day->month, $day->day );
        push @points,
          [ "$y-$m-$d", num_published_month_before( $y, $m, $d, $home_wiki ) ];
    }
    return \@points;
}

ajax '/chart/:start_year?/:end_year?/:interval?/:home_wiki' => sub {

    # get years range input
    my $start_year = param('start_year') ? param('start_year') : 2005;
    my $end_year = param('end_year') ? param('end_year') : DateTime->now->year;

    # get interval input
    my $interval = param('interval') ? param('interval') : 10;    # days
           # retrieve the data and return the json
    my $result = to_json get_chart_points( [ $start_year .. $end_year ],
        $interval, param('home_wiki') );
    return $result;
};

get '/' => sub {
    template 'index';
};

true;
